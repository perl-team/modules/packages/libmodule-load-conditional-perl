libmodule-load-conditional-perl (0.74-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmodule-load-conditional-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 06:05:22 +0100

libmodule-load-conditional-perl (0.74-1) unstable; urgency=medium

  * Import upstream version 0.74.

 -- gregor herrmann <gregoa@debian.org>  Tue, 25 Aug 2020 02:06:37 +0200

libmodule-load-conditional-perl (0.72-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.72.
  * Update years of packaging copyright.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Jun 2020 20:49:32 +0200

libmodule-load-conditional-perl (0.70-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Remove Alejandro Garrido Mota from Uploaders. Thanks for your work!
  * Remove Daniel Kahn Gillmor from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Nick Morrott ]
  * New upstream version 0.70
  * d/compat:
    - Drop file
  * d/control:
    - Declare compliance with Debian Policy 4.4.1 (no changes)
    - Bump debhelper compatibility level to 12
    - Refresh (build) dependencies
    - Add Rules-Requires-Root field
  * d/copyright:
    - Update Debian Files stanza
  * d/u/metadata:
    - Refresh metadata
  * d/watch:
    - Migrate to version 4 of the watch file format

 -- Nick Morrott <nickm@debian.org>  Tue, 12 Nov 2019 10:24:05 +0000

libmodule-load-conditional-perl (0.68-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Imported Upstream version 0.68
  * Add debian/upstream/metadata
  * Update upstream contact details
  * Bump debhelper compatibility to version 9
  * Bump Standards-Version to 3.9.8 (no changes)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 29 Jul 2016 14:50:51 +0100

libmodule-load-conditional-perl (0.64-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Imported Upstream version 0.64
    Closes: #788261 -- uninstallable with perl 5.22
  * Add Testsuite header
  * Delcare conformance to Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Sat, 13 Jun 2015 15:20:43 +0000

libmodule-load-conditional-perl (0.62-1) unstable; urgency=medium

  * New upstream releases 0.60, 0.62.
  * Update years of packaging copyright.
  * Update (build) dependencies.
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Aug 2014 19:26:06 +0200

libmodule-load-conditional-perl (0.58-1) unstable; urgency=low

  * New upstream release
  * Adding myself to uploaders

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 10 Sep 2013 02:12:18 -0400

libmodule-load-conditional-perl (0.54-1) UNRELEASED; urgency=low

  [ gregor herrmann ]
  IGNORE-VERSION: 0.54-1
  # only change:
  # VMS test fixes

  * New upstream release.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Aug 2012 21:50:30 +0200

libmodule-load-conditional-perl (0.52-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Nuno Carvalho ]
  * d/control: Add new dependency libmodule-metadata-perl.
  * New upstream release.

 -- Nuno Carvalho <smash@cpan.org>  Sat, 04 Aug 2012 21:44:56 +0100

libmodule-load-conditional-perl (0.50-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 20:53:06 +0200

libmodule-load-conditional-perl (0.48-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Set debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 Mar 2012 20:32:36 +0100

libmodule-load-conditional-perl (0.46-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nicholas Bamber ]
  * New upstream release
  * Raised standards version to 3.9.2
  * Removed unnecessary versioned dependency

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 09 Sep 2011 09:22:53 +0100

libmodule-load-conditional-perl (0.44-1) unstable; urgency=low

  * New upstream release
  * debian/control: Removed spurious notice about deprecation

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Thu, 10 Feb 2011 21:07:49 +0000

libmodule-load-conditional-perl (0.40-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Nicholas Bamber ]
  * New upstream release
  * Add myself to Uploaders
  * Upped standards version to 3.9.1
  * Refreshed copyright

  [ gregor herrmann ]
  * debian/control: remove "perl (>= 5.10) | libversion-perl", satisfied in
    lenny.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 08 Jan 2011 22:08:48 +0000

libmodule-load-conditional-perl (0.38-1) unstable; urgency=low

  * New upstream release.
  * Use source format 3.0 (quilt).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 25 Apr 2010 12:00:34 +0900

libmodule-load-conditional-perl (0.36-1) unstable; urgency=low

  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Update copyright to DEP5 format

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 09 Feb 2010 23:04:35 -0500

libmodule-load-conditional-perl (0.34-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 0.32
  * Refreshed copyright information
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Use new short debhelper rules format
  * Added myself to Uploaders and Copyright
  * Add a README.Debian to note this module is DEPRECATED

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * New upstream release 0.34.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Oct 2009 15:48:22 +0100

libmodule-load-conditional-perl (0.30-1) unstable; urgency=low

  * New upstream release
  * debian/copyright:
    + Add upstream copyright year
    + Separete debian/* copyright
  * debian/control: add myself to uploaders

 -- Rene Mayorga <rmayorga@debian.org>  Tue, 20 Jan 2009 18:20:12 -0600

libmodule-load-conditional-perl (0.28-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - switch Vcs-Browser field to ViewSVN
    - change my email address
    - add perl-modules (>= 5.10) as an alternative (build)
      dependency to libversion-perl.
  * Set Standards-Version to 3.8.0 (no changes).
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * debian/copyright: switch to new format.

 -- gregor herrmann <gregoa@debian.org>  Thu, 18 Dec 2008 21:53:31 +0100

libmodule-load-conditional-perl (0.26-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Tweak regex.

 -- Roberto C. Sanchez <roberto@debian.org>  Sun, 16 Mar 2008 02:07:31 -0400

libmodule-load-conditional-perl (0.24-2) unstable; urgency=medium

  * Remove alternative (build) dependency on perl-modules and make (build)
    dependency on libversion-perl unversioned; thanks Niko for the research
    (closes: #464302).
  * Set urgency to medium because of the fix for a FTBFS bug.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 12 Feb 2008 23:05:16 +0100

libmodule-load-conditional-perl (0.24-1) unstable; urgency=low

  [ Alejandro Garrido Mota ]
  * New upstream version
  * Move package to pkg-perl project
  * New upstream version bug (Closes: #459695)
  * Change rules to use cdbs instead debhelper

  [ Martín Ferrari ]
  * Fixed typo in debian/control.
  * Added debian/watch file.

  [ gregor herrmann ]
  * debian/control: Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Debian Perl Project
    <pkg-perl-maintainers@lists.alioth.debian.org>).

  [ Damyan Ivanov ]
  * Use fresh rules.MakeMaker.noxs for debian/rules
  * debhelper compatibility level 6
  * add myself to Uploaders
  * add perl-modules (>> 5.10) (build-) dependency as alternative to
    libversion (>= 0.69)

 -- Damyan Ivanov <dmn@debian.org>  Thu, 24 Jan 2008 11:06:57 +0200

libmodule-load-conditional-perl (0.16-2) unstable; urgency=low

  * Add upstream webpage in copyright file

 -- Alejandro Garrido Mota <garridomota@gmail.com>  Tue, 12 Jun 2007 17:56:54 -0400

libmodule-load-conditional-perl (0.16-1) unstable; urgency=low

  * Initial Release (Closes: #426717).

 -- Alejandro Garrido Mota <garridomota@gmail.com>  Wed, 30 May 2007 10:10:43 -0400
